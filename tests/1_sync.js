const assert = require('assert');
const moolb = require('moolb');

const sync = require('../sync');

describe('sync', () => {

	it('blocks_tx', () => {
		const filter_blocks = new moolb.Bloom(8192, 3);
		const filter_blocktxs = new moolb.Bloom(8192, 3);

		let b = new ArrayBuffer(4);
		let w = new DataView(b);
		w.setUint32(0, 42);

		filter_blocks.add(new Uint8Array(b));

		b = new ArrayBuffer(8);
		w = new DataView(b);
		w.setUint32(0, 42);
		w.setUint32(4, 13);
		filter_blocktxs.add(new Uint8Array(b));

		const countGetter = (n) => {
			return new Promise((yay, nay) => {
				yay(100);
			});	
		}

		sync.by_filter(0, 50, filter_blocks, filter_blocktxs, countGetter, (r, t) => {
			assert.equals(r, 42);
			assert.equals(r, 42);
		});
	});
});
