const assert = require('assert');
const Web3 = require('web3');
const moolb = require('moolb');

const driver = require('../driver');
const sync = require('../sync');

describe('ondemand', () => {

	it('driver', () => {
		const w3 = new Web3('http://localhost:8545');

		const callback = (r) => {
			assert.equal(r.blockNumber, 2);
			assert.equal(r.transactionIndex, 0);
		}

		const filter_blocks = new moolb.Bloom(8192, 3);
		const filter_blocktxs = new moolb.Bloom(8192, 3);

		let b = new ArrayBuffer(4);
		let w = new DataView(b);
		w.setUint32(0, 2);

		filter_blocks.add(new Uint8Array(b));

		b = new ArrayBuffer(8);
		w = new DataView(b);
		w.setUint32(0, 2);
		w.setUint32(4, 0);
		filter_blocktxs.add(new Uint8Array(b));

		const drv = new driver.Driver(w3, 0, [filter_blocks, filter_blocktxs], sync.by_filter, callback);

		drv.start();
	});

});

