const window = self;

self.importScripts(
	'driver.js',
	'web3.min.js',
);

async function sync(driver) {
	driver.w3.eth.subscribe('newBlockHeaders', async function(e, r) {
		const c = await driver.w3.eth.getBlockTransactionCount(r.number);
		for (let i = 0; i < c; i++) {
			console.log('driver process ', r.number, i);
			driver.process(r.number, i);
		}
	});
}

onmessage = function(o) {
	const w3 = new Web3(o.data.w3_provider);

	const callback = (o) => {
		this.postMessage(o);
	};

	w3.eth.getBlockNumber().then(function(n) {
		const driver = new Driver(w3, n, undefined, undefined, callback);
		sync(driver);
	});
};
