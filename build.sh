#!/bin/bash
set -e
mkdir -p dist
cp node_modules/moolb/moolb/moolb.js dist/
cp node_modules/web3/dist/web3.min.js dist/
cp src/sync.js dist/
cp src/driver.js dist/
cp src/workers/ondemand.js dist/worker_ondemand.js
cp src/workers/head.js dist/worker_head.js
set +e
